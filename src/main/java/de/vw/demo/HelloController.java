package de.vw.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/calculate")
    public String calculateNumbers(@RequestParam(required = true) int num1, @RequestParam(required = true) int num2, @RequestParam(required = true) String op) {

        int result = 0;

        if (op == "+") {
            return String.valueOf(num1 + num2);
        }
        if (op == "-") {
            return String.valueOf(num1 - num2);
        }
        if (op == "*") {
            return String.valueOf(num1 * num2);
        }
        if (op == "/") {
            return String.valueOf(num1 / num2);
        }

        return String.valueOf("There is nothing to calculate!");

    }

    @GetMapping("/countVowels")
    public String countVowels(@RequestParam(required = true) String text) {


        int count = (int) text.chars().mapToObj(ch -> (char)ch).filter(c -> c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ).count();

        return String.valueOf(count);
    }
}
