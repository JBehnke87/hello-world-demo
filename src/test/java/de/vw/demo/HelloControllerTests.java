package de.vw.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noArgs_returnsHelloWorld() throws Exception {
        mockMvc.perform(get("/hello"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }

    @Test
    void sayHello_myName_returnsHelloName() throws Exception {
        mockMvc.perform(get("/hello?name=Jan"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Jan"));
    }

    @Test
    void getNumbersAndCalculate() throws Exception {
        mockMvc.perform(get("/calculate")
                .param("num1", "5")
                .param("num2", "4")
                .param("op", "*"))
                .andExpect(status().isOk())
                .andExpect(content().string("20"));
    }

    @Test
    void countVowels() throws Exception {
        mockMvc.perform(get("/countVowels")
                .param("text", "helloTest"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));

    }
}

